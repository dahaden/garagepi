#!/bin/bash

#/bin/ip addr >/tmp/my_script.out

echo "Waiting for network"

while ! ifconfig | grep -F "192.168.68.112" > /dev/null; do
  sleep 1;
done

#echo "Checking network"

#ifconfig | grep "192.168.68." -C 3 > /tmp/garage_pi_start.log

echo "Starting GaragePi webservice"

./target/debug/garage_pi
