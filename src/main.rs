extern crate actix_web;
extern crate rppal;
#[macro_use] extern crate log;
extern crate simplelog;
extern crate chrono;

use actix_files as fs;
use actix_web::http::{StatusCode};
use actix_web::{
    middleware, App, HttpServer, HttpResponse, HttpRequest,
    Result, web
};
use serde_derive::{Deserialize, Serialize};

use rppal::gpio::{
    Gpio, Level,
};
use std::thread;
use std::time::{
    Duration, SystemTime
};
use simplelog::{
    WriteLogger,
    LevelFilter,
    Config
};
use chrono::offset::Utc;
use chrono::DateTime;
use std::fs::{
    File, create_dir
};

use std::path::Path;


/// simple index handler
fn garage_open(req: HttpRequest) -> Result<HttpResponse> {
    debug!("{:?}", req);
    info!("Opening garage");

    // session
    // let mut counter = 1;
    // if let Some(count) = session.get::<i32>("counter")? {
    //     println!("SESSION value: {}", count);
    //     counter = count + 1;
    // }

    // // set counter to session
    // session.set("counter", counter)?;

    // response
    let relay_result = open_relay();
    match relay_result {
        Ok(()) => Ok(HttpResponse::build(StatusCode::NO_CONTENT).finish()),
        Err(e) => {
            error!("Failed to open relay: {}", e);
            Ok(HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).body(format!("Failed to trigger relay")))
        },
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct GarageState {
    is_open: bool,
}

fn garage_state(req: HttpRequest) -> Result<HttpResponse> {
    debug!("{:?}", req);
    info!("Getting garage state");

    match is_garage_open() {
        Ok(value) => {
            let state = GarageState {
                is_open: value
            };
            Ok(HttpResponse::Ok().json(state))
        },
        Err(e) => {
            error!("Failed to open relay: {}", e);
            Ok(HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).body(format!("Failed to trigger relay")))
        }
    }    
}

fn is_garage_open() -> rppal::gpio::Result<bool> {
    let switch_pin = Gpio::new()?.get(17)?.into_input();
    match switch_pin.read() {
        Level::High => Ok(false),
        Level::Low => Ok(true),
    }
}

fn open_relay() -> rppal::gpio::Result<()>  {
    let mut relay_pin = Gpio::new()?.get(4)?.into_output();
    //println!("Pin direction set");
    relay_pin.set_high();
    debug!("Relay closed");
    debug!("Sleeping");
    thread::sleep(Duration::from_secs(2));
    debug!("Opening relay");
    relay_pin.set_low();
    Ok(())
}

fn main() -> std::io::Result<()> {

    if !Path::new("./log/").exists() {
        match create_dir("./log") {
            Err(e) => {
                println!("Failed to create or detect log dir: {}", e);
                panic!("");
            },
            Ok(()) => {}
        };
    }

    let system_time = SystemTime::now();
    let datetime: DateTime<Utc> = system_time.into();
    let formated_date_time = datetime.format("%Y_%m_%d__%H_%M_%S");
    let file_name = format!("./log/{}.log", formated_date_time);
    let file = File::create(file_name).unwrap();
    WriteLogger::init(LevelFilter::Info, Config::default(), file).unwrap();

    info!("Starting application");

    std::env::set_var("RUST_LOG", "actix_web=info");

    HttpServer::new(|| {
        App::new()
            // enable logger
            .wrap(middleware::Logger::default())
            .service(web::resource("/api/garage/door").route(web::delete().to(garage_open)))
            .service(web::resource("/api/garage/state").route(web::get().to(garage_state)))
            .service(
                // static files
                fs::Files::new("/", "./static/").index_file("index.html"),
            )            
    })
    .bind("192.168.68.112:8080")?
    .run()
}
