#!/bin/bash

echo "Moving to root of project"
pushd /home/pi/src/garagepi

echo "Pulling latest from bitbucket"
git pull

echo "Building"
cargo build



if ($(whoami) != root)
  then echo "Please run sudo `systemctl restart garage-pi` to restart garage_pi"

  else
    echo "Restarting service"
    systemctl restart garage-pi
fi
