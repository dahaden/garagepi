function initApplication() {
  const button = document.getElementById("open-sesame");
  button.addEventListener('click', event => {
    button.innerHTML = `Click count: ${event.detail}`;
    fetch("/api/garage/door", { method: "DELETE" })
      .then(() => console.log("Success"))
      .catch(err => console.error("Error:", err));
  });
  pollIsOpen();
};

document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    initApplication();
  }
}

function pollIsOpen() {
  const span = document.getElementById("is_open");
  setInterval(() => {
    fetch("/api/garage/state")
      .then(response => response.json())
      .then(body => body.is_open)
      .then(isOpen => {
        if (isOpen) { 
          span.innerHTML = "open";
        } else {
          span.innerHTML = "closed";
        }
      });
    },
    2000
  );
}
